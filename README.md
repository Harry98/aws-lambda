This repo contains terraform files that configure a AWS API GateWay and a Lambda function.
Also contains python files that receive API requests and return a greeting message in HTTP format.

First run in the command line to create a bucket:
$ aws s3api create-bucket --bucket=python-function --region=us-east-1

Add the two python functions to S3:
$ aws s3 cp Hello.zip s3://python-function/v1/Hello.zip
$ aws s3 cp Holiday.zip s3://python-function/v2/Holiday.zip

Run "terraform init" under tf directory and run either of the following two commands:

$ terraform apply -var="greeting=1" -- returns a url that shows "Hello World!"
$ terraform apply -var="greeting=2" -- returns a url that greets "Hello World!" followed by holiday greetings.

Example url looks like: https://g35mmprrq2.execute-api.us-east-1.amazonaws.com/test

